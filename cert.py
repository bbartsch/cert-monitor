import datetime
from socket import socket
import sys
import OpenSSL

class Cert():    
    
    def __init__(self, url):
        self.url = url
        self.dateformat = "%d-%m-%Y"

        client = socket()
        client.connect((self.url, 443))
        
        client_ssl = OpenSSL.SSL.Connection(OpenSSL.SSL.Context(OpenSSL.SSL.SSLv23_METHOD), client)
        client_ssl.set_connect_state()
        client_ssl.set_tlsext_host_name(self.url.encode())
        client_ssl.do_handshake()
        self.cert = client_ssl.get_peer_certificate()
        client_ssl.close()

    def get_expire_date(self):
        cert_date = self.cert.get_notAfter()
        exp_day = cert_date[6:8].decode('utf-8')
        exp_month = cert_date[4:6].decode('utf-8')
        exp_year = cert_date[:4].decode('utf-8')

        exp_date = str(exp_day) + "-" + str(exp_month) + "-" + str(exp_year)        
        return datetime.datetime.strptime(exp_date, self.dateformat)

    def is_expired(self):
        return self.cert.has_expired()
    
    def get_expire_in_days(self):
        return (self.get_expire_date() - datetime.datetime.now()).days
    
    def get_issuer(self):
        # .O is a var for organisations
        return self.cert.get_issuer().O

if __name__ == "__main__":
    domain = sys.argv[1]
    print(Cert(domain).get_issuer())
    
